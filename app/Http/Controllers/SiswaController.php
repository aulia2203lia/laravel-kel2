<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Siswa;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('search')) {
            $data = Siswa::where('nama', 'LIKE', '%' . $request->search . '%')->paginate(5);
        } else {
            $data = Siswa::with('kelas')->paginate(5);
        }
        return view('datasiswa', compact('data'));
    }

    public function tambahsiswa()
    {
        $kel = Kelas::all();
        return view('tambahdatasiswa', compact('kel'));
    }

    public function insertsiswa(Request $request)
    {
        //dd($request->all());
        Siswa::create($request->all());
        return redirect()->route('siswa')->with('success', 'Data siswa berhasil ditambahkan');
    }

    public function tampilkandatasiswa($id)
    {
        $data = Siswa::find($id);
        $kel = Kelas::all();
        //dd($data);
        return view('tampildatasiswa', compact('data', 'kel'));
    }

    public function updatedatasiswa(Request $request, $id)
    {
        $data = Siswa::find($id);
        $data->update($request->all());
        return redirect()->route('siswa')->with('success', 'Data siswa berhasil di update');
    }

    public function deletedatasiswa($id)
    {
        $data = Siswa::find($id);
        $data->delete();
        return redirect()->route('siswa')->with('success', 'Data siswa berhasil dihapus');
    }
}

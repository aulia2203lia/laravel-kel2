<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('search')) {
            $data = User::where('name', 'LIKE', '%' . $request->search . '%')->paginate(5);
        } else {
            $data = User::paginate(5);
        }
        return view('datauser', compact('data'));
    }

    public function tambahuser()
    {
        return view('tambahdatauser');
    }

    public function insertuser(Request $request)
    {
        //dd($request->all());
        User::create($request->all());
        return redirect()->route('user')->with('success', 'Data user berhasil ditambahkan');
    }

    public function tampilkandatauser($id)
    {
        $data = User::find($id);
        //dd($data);
        return view('tampildatauser', compact('data'));
    }

    public function updatedatauser(Request $request, $id)
    {
        $data = User::find($id);
        $data->update($request->all());
        return redirect()->route('user')->with('success', 'Data user berhasil di update');
    }

    public function deletedatauser($id)
    {
        $data = User::find($id);
        $data->delete();
        return redirect()->route('user')->with('success', 'Data user berhasil dihapus');
    }
}

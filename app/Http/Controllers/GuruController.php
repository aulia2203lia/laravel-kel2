<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Guru;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class GuruController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('search')) {
            $data = Guru::where('nama', 'LIKE', '%' . $request->search . '%')->paginate(5);
        } else {
            $data = Guru::with('user')->paginate(5);
        }
        return view('dataguru', compact('data'));
    }

    public function tambahguru()
    {
        $data = Guru::all();
        $user = User::all();
        return view('tambahdataguru', compact('data', 'user'));
    }

    public function insertguru(Request $request)
    {
        //dd($request->all());
        Guru::create($request->all());
        return redirect()->route('guru')->with('success', 'Data guru berhasil ditambahkan');
    }

    public function tampilkandataguru($id)
    {
        $data = Guru::find($id);
        $user = User::all();
        //dd($data);
        return view('tampildataguru', compact('data', 'user'));
    }

    public function updatedataguru(Request $request, $id)
    {
        $data = Guru::find($id);
        $data->update($request->all());
        return redirect()->route('guru')->with('success', 'Data guru berhasil di update');
    }

    public function deletedataguru($id)
    {
        $data = Guru::find($id);
        $data->delete();
        return redirect()->route('guru')->with('success', 'Data guru berhasil dihapus');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Mapel;
use Illuminate\Http\Request;

class MapelController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('search')) {
            $data = Mapel::where('nama_mapel', 'LIKE', '%' . $request->search . '%')->paginate(5);
        } else {
            $data = Mapel::paginate(5);
        }
        return view('datamapel', compact('data'));
    }

    public function tambahmapel()
    {
        return view('tambahdatamapel');
    }

    public function insertmapel(Request $request)
    {
        //dd($request->all());
        Mapel::create($request->all());
        return redirect()->route('mapel')->with('success', 'Data mapel berhasil ditambahkan');
    }

    public function tampilkandatamapel($id)
    {
        $data = Mapel::find($id);
        //dd($data);
        return view('tampildatamapel', compact('data'));
    }

    public function updatedatamapel(Request $request, $id)
    {
        $data = Mapel::find($id);
        $data->update($request->all());
        return redirect()->route('mapel')->with('success', 'Data mapel berhasil di update');
    }

    public function deletedatamapel($id)
    {
        $data = Mapel::find($id);
        $data->delete();
        return redirect()->route('mapel')->with('success', 'Data mapel berhasil dihapus');
    }
}

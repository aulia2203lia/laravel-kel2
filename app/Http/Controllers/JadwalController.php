<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Guru;
use App\Models\Mapel;
use App\Models\Jadwal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class JadwalController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('search')) {
            $data = Jadwal::where('hari', 'LIKE', '%' . $request->search . '%')->paginate(5);
        } else {
            $data = Jadwal::with('kelas', 'mapel', 'guru')->paginate(5);
        }
        return view('datajadwal', compact('data'));
    }

    public function tambahjadwal()
    {
        $data = Jadwal::all();
        $kelas = Kelas::all();
        $mapel = Mapel::all();
        $guru = Guru::all();
        return view('tambahdatajadwal', compact('data', 'kelas', 'mapel', 'guru'));
    }

    public function insertjadwal(Request $request)
    {
        //dd($request->all());
        Jadwal::create($request->all());
        return redirect()->route('jadwal')->with('success', 'Data jadwal berhasil ditambahkan');
    }

    public function tampilkandatajadwal($id)
    {
        $data = Jadwal::find($id);
        $kelas = Kelas::all();
        $mapel = Mapel::all();
        $guru = Guru::all();
        //dd($data);
        return view('tampildatajadwal', compact('data', 'kelas', 'mapel', 'guru'));
    }

    public function updatedatajadwal(Request $request, $id)
    {
        $data = Jadwal::find($id);
        $data->update($request->all());
        return redirect()->route('jadwal')->with('success', 'Data jadwal berhasil di update');
    }

    public function deletedatajadwal($id)
    {
        $data = Jadwal::find($id);
        $data->delete();
        return redirect()->route('jadwal')->with('success', 'Data jadwal berhasil dihapus');
    }
}

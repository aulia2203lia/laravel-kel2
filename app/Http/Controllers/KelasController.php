<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use Illuminate\Http\Request;

class KelasController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('search')) {
            $data = Kelas::where('nama_kelas', 'LIKE', '%' . $request->search . '%')->paginate(5);
        } else {
            $data = Kelas::paginate(5);
        }
        return view('datakelas', compact('data'));
    }

    public function tambahkelas()
    {
        return view('tambahdatakelas');
    }

    public function insertkelas(Request $request)
    {
        //dd($request->all());
        Kelas::create($request->all());
        return redirect()->route('kelas')->with('success', 'Data kelas berhasil ditambahkan');
    }

    public function tampilkandatakelas($id)
    {
        $data = Kelas::find($id);
        //dd($data);
        return view('tampildatakelas', compact('data'));
    }

    public function updatedatakelas(Request $request, $id)
    {
        $data = Kelas::find($id);
        $data->update($request->all());
        return redirect()->route('kelas')->with('success', 'Data kelas berhasil di update');
    }

    public function deletedatakelas($id)
    {
        $data = Kelas::find($id);
        $data->delete();
        return redirect()->route('kelas')->with('success', 'Data kelas berhasil dihapus');
    }
}

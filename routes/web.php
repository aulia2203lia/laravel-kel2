<?php

use App\Models\Guru;
use App\Models\Siswa;
use App\Models\Mapel;
use App\Models\Jadwal;
use App\Models\Kelas;

use App\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GuruController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\MapelController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use Illuminate\Routing\RouteGroup;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//login
Route::get('/', function () {
    return view('pengguna.login');
})->name('login');

Route::get('/login', function () {
    return view('pengguna.login');
})->name('login');
Route::post('/postlogin', 'LoginController@postlogin')->name('postlogin');
Route::get('/logout', 'LoginController@logout')->name('logout');
//akhir login

Route::get('/home', function () {
    $jumlahguru = Guru::count();
    $jumlahsiswa = Siswa::count();
    $jumlahmapel = Mapel::count();
    $jumlahkelas = Kelas::count();
    return view('home', compact('jumlahguru', 'jumlahsiswa', 'jumlahmapel', 'jumlahkelas'));
});


Route::group(['middleware' => ['auth', 'ceklevel:admin']], function () {

    //user
    Route::get('/user', [UserController::class, 'index'])->name('user');
    Route::get('/tambahuser', [UserController::class, 'tambahuser'])->name('tambahuser');
    Route::post('/insertuser', [UserController::class, 'insertuser'])->name('insertuser');
    Route::get('/tampilkandatauser/{id}', [UserController::class, 'tampilkandatauser'])->name('tampilkandatauser');
    Route::post('/updatedatauser/{id}', [UserController::class, 'updatedatauser'])->name('updatedatauser');
    Route::get('/deletedatauser/{id}', [UserController::class, 'deletedatauser'])->name('deletedatauser');
    //akhir user
});

Route::group(['middleware' => ['auth', 'ceklevel:guru,admin']], function () {

    //guru
    Route::get('/guru', [GuruController::class, 'index'])->name('guru');
    Route::get('/tambahguru', [GuruController::class, 'tambahguru'])->name('tambahguru');
    Route::post('/insertguru', [GuruController::class, 'insertguru'])->name('insertguru');
    Route::get('/tampilkandataguru/{id}', [GuruController::class, 'tampilkandataguru'])->name('tampilkandataguru');
    Route::post('/updatedataguru/{id}', [GuruController::class, 'updatedataguru'])->name('updatedataguru');
    Route::get('/deletedataguru/{id}', [GuruController::class, 'deletedataguru'])->name('deletedataguru');
    //akhir guru

    //siswa
    Route::get('/siswa', [SiswaController::class, 'index'])->name('siswa');
    Route::get('/tambahsiswa', [SiswaController::class, 'tambahsiswa'])->name('tambahsiswa');
    Route::post('/insertsiswa', [SiswaController::class, 'insertsiswa'])->name('insertsiswa');
    Route::get('/tampilkandatasiswa/{id}', [SiswaController::class, 'tampilkandatasiswa'])->name('tampilkandatasiswa');
    Route::post('/updatedatasiswa/{id}', [SiswaController::class, 'updatedatasiswa'])->name('updatedatasiswa');
    Route::get('/deletedatasiswa/{id}', [SiswaController::class, 'deletedatasiswa'])->name('deletedatasiswa');
    //akhir siswa

    //mapel
    Route::get('/mapel', [MapelController::class, 'index'])->name('mapel');
    Route::get('/tambahmapel', [MapelController::class, 'tambahmapel'])->name('tambahmapel');
    Route::post('/insertmapel', [MapelController::class, 'insertmapel'])->name('insertmapel');
    Route::get('/tampilkandatamapel/{id}', [MapelController::class, 'tampilkandatamapel'])->name('tampilkandatamapel');
    Route::post('/updatedatamapel/{id}', [MapelController::class, 'updatedatamapel'])->name('updatedatamapel');
    Route::get('/deletedatamapel/{id}', [MapelController::class, 'deletedatamapel'])->name('deletedatamapel');
    //akhir mapel

    //kelas
    Route::get('/kelas', [KelasController::class, 'index'])->name('kelas');
    Route::get('/tambahkelas', [KelasController::class, 'tambahkelas'])->name('tambahkelas');
    Route::post('/insertkelas', [KelasController::class, 'insertkelas'])->name('insertkelas');
    Route::get('/tampilkandatakelas/{id}', [KelasController::class, 'tampilkandatakelas'])->name('tampilkandatakelas');
    Route::post('/updatedatakelas/{id}', [KelasController::class, 'updatedatakelas'])->name('updatedatakelas');
    Route::get('/deletedatakelas/{id}', [KelasController::class, 'deletedatakelas'])->name('deletedatakelas');
    //akhir kelas


    //jadwal
    Route::get('/jadwal', [JadwalController::class, 'index'])->name('jadwal');
    Route::get('/tambahjadwal', [JadwalController::class, 'tambahjadwal'])->name('tambahjadwal');
    Route::post('/insertjadwal', [JadwalController::class, 'insertjadwal'])->name('insertjadwal');
    Route::get('/tampilkandatajadwal/{id}', [JadwalController::class, 'tampilkandatajadwal'])->name('tampilkandatajadwal');
    Route::post('/updatedatajadwal/{id}', [JadwalController::class, 'updatedatajadwal'])->name('updatedatajadwal');
    Route::get('/deletedatajadwal/{id}', [JadwalController::class, 'deletedatajadwal'])->name('deletedatajadwal');
    //akhir jadwal

});

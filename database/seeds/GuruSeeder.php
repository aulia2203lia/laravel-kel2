<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GuruSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('guru')->insert([
            'user_id' => 'admin',
            'nis' => '18120001',
            'nama' => 'Lili Ya',
            'tempat_lahir' => 'Magelang',
            'tgl_lahir' => '1993-02-21',
            'gender' => 'perempuan',
            'phone_number' => '081873654',
            'email' => 'lili@gmail.com',
            'alamat' => 'Perum Puri Cendana',
            'pendidikan' => 'S1 Sosiologi',
        ]);
    }
}

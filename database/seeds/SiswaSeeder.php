<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('siswa')->insert([
            'nis' => '2105040034',
            'nama' => 'Aulia',
            'gender' => 'perempuan',
            'tempat_lahir' => 'Bekasi',
            'tgl_lahir' => '2001-12-21',
            'email' => 'lia@gmail.com',
            'nama_ortu' => 'Lili',
            'alamat' => 'Perum Puri Cendana',
            'phone_number' => '081873654',
            'kelas_id' => '131',
        ]);
    }
}

@extends('layout.admin')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Welcome</h1>
                </div><!-- /.col -->
                <!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">

                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Jumlah Guru</span>
                            <span class="info-box-number">
                                {{ $jumlahguru }}
                                <small>Guru</small>
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->


                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-school"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Jumlah Siswa</span>
                            <span class="info-box-number">
                                {{ $jumlahsiswa }}
                                <small>Siswa</small>
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <!-- fix for small devices only -->
                <div class="clearfix hidden-md-up"></div>

                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-landmark"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Jumlah Kelas</span>
                            <span class="info-box-number">
                                {{ $jumlahkelas }}
                                <small>Kelas</small>
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-th"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Jumlah Mapel</span>
                            <span class="info-box-number">
                                {{ $jumlahmapel }}
                                <small>Mapel</small>
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->



            <!-- lain lain-->
            <h5 class="mb-2">Informasi</h5>
            <div class="card card-success">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-6 col-xl-4">
                            <div class="card mb-2 bg-gradient-dark">
                                <img class="card-img-top" src="{{ asset('template/dist/img/photo1.png') }}" alt="Dist Photo 1">
                                <div class="card-img-overlay">
                                    <h5 class="card-title text-primary">Card Title</h5>
                                    <p class="card-text pb-1 pt-1 text-white">
                                        Lorem ipsum dolor <br>
                                        sit amet, consectetur <br>
                                        adipisicing elit sed <br>
                                        do eiusmod tempor. </p>
                                    <a href="#" class="text-primary">Last update 3 days ago</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-4">
                            <div class="card mb-2">
                                <img class="card-img-top" src="{{ asset('template/dist/img/photo2.png') }}" alt="Dist Photo 2">
                                <div class="card-img-overlay">
                                    <h5 class="card-title text-primary">Card Title</h5>
                                    <p class="card-text pb-1 pt-1 text-white">
                                        Lorem ipsum dolor <br>
                                        sit amet, consectetur <br>
                                        adipisicing elit sed <br>
                                        do eiusmod tempor. </p>
                                    <a href="#" class="text-primary">Last update 3 days ago</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-4">
                            <div class="card mb-2">
                                <img class="card-img-top" src="{{ asset('template/dist/img/photo3.jpg') }}" alt="Dist Photo 3">
                                <div class="card-img-overlay">
                                    <h5 class="card-title text-primary">Card Title</h5>
                                    <p class="card-text pb-1 pt-1 text-white">
                                        Lorem ipsum dolor <br>
                                        sit amet, consectetur <br>
                                        adipisicing elit sed <br>
                                        do eiusmod tempor. </p>
                                    <a href="#" class="text-primary">Last update 3 days ago</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- akhir lain lain-->
            </div>
            <!-- /.card-footer -->
        </div>
        <!-- /.card -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</div>
<!--/. container-fluid -->
</section>
<!-- /.content -->
</div>
@endsection
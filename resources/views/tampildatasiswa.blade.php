@extends('layout.admin')

@section('content')

<body>
    <h1 class="text-center mb-4">Edit Data Siswa</h1>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8">
                <!--input-->
                <div class="card">
                    <div class="card-body">
                        <form action="/updatedatasiswa/{{ $data->id }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">NIS</label>
                                <input type="text" name="nis" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->nis }}">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Nama</label>
                                <input type="text" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->nama }}">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Gender</label>
                                <select class="form-select" name="gender" aria-label="Default select example">
                                    <option selected>{{ $data->gender }}</option>
                                    <option value="1">laki-laki</option>
                                    <option value="2">perempuan</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->tempat_lahir }}">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Tanggal Lahir</label>
                                <input type="date" name="tgl_lahir" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->tgl_lahir }}">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Email</label>
                                <input type="text" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->email }}">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Nama Ortu</label>
                                <input type="text" name="nama_ortu" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->nama_ortu }}">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Alamat</label>
                                <input type="text" name="alamat" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->alamat }}">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Nomer Telp</label>
                                <input type="number" name="phone_number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->phone_number }}">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputJK" class="form-label">Kelas</label>
                                <select class="form-select form-control" name="kelas_id" id="kelas_id" aria-label="Default select example">
                                    <option disabled value>Pilih Kelas</option>
                                    @foreach ($kel as $row)
                                    <option value="{{ $row->id }}">{{ $row->nama_kelas}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>

                            <br>
                        </form>
                        <br>
                    </div>
                </div>
                <br>
                <br>
                <!--akhir input-->
            </div>
        </div>
    </div>



    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

@endsection
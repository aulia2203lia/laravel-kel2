@extends('layout.admin')

@section('content')

<body>
    <h1 class="text-center mb-4">Edit Data Kelas</h1>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8">
                <!--input-->
                <div class="card">
                    <div class="card-body">
                        <form action="/updatedatakelas/{{ $data->id }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Kode Kelas</label>
                                <input type="text" name="kode_kelas" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->kode_kelas }}">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Nama Kelas</label>
                                <input type="text" name="nama_kelas" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->nama_kelas }}">
                            </div>
                            <div class="mb-3"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>

                            <br>
                        </form>
                        <br>
                    </div>
                </div>
                <br>
                <br>
                <!--akhir input-->
            </div>
        </div>
    </div>



    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

@endsection
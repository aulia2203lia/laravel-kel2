@extends('layout.admin')

@section('content')

<body>
    <h1 class="text-center mb-4">Edit Data Jadwal</h1>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8">
                <!--input-->
                <div class="card">
                    <div class="card-body">
                        <form action="/updatedatajadwal/{{ $data->id }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <label for="exampleInputJK" class="form-label">Kelas</label>
                                <select class="form-select form-control" name="kelas_id" id="kelas_id" aria-label="Default select example">
                                    <option disabled value>Pilih Kelas</option>
                                    @foreach ($kelas as $row)
                                    <option value="{{ $row->id }}">{{ $row->nama_kelas}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputJK" class="form-label">Mapel</label>
                                <select class="form-select form-control" name="mapel_id" id="mapel_id" aria-label="Default select example">
                                    <option disabled value>Pilih Mapel</option>
                                    @foreach ($mapel as $row)
                                    <option value="{{ $row->id }}">{{ $row->nama_mapel}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputJK" class="form-label">Guru</label>
                                <select class="form-select form-control" name="guru_id" id="guru_id" aria-label="Default select example">
                                    <option disabled value>Pilih Guru</option>
                                    @foreach ($guru as $row)
                                    <option value="{{ $row->id }}">{{ $row->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Hari</label>
                                <select class="form-select" name="hari" aria-label="Default select example">
                                    <option selected>{{ $data->hari }}</option>
                                    <option value="1">Senin</option>
                                    <option value="2">Selasa</option>
                                    <option value="3">Rabu</option>
                                    <option value="4">Kamis</option>
                                    <option value="5">Jumat</option>
                                    <option value="6">Sabtu</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Jam Pelajaran</label>
                                <input type="text" name="jam_pelajaran" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->jam_pelajaran }}">
                            </div>
                            <div class="mb-3"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>

                            <br>
                        </form>
                        <br>
                    </div>
                </div>
                <br>
                <br>
                <!--akhir input-->
            </div>
        </div>
    </div>



    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

@endsection